#include <iostream>
#include <cstdlib>
#include <Windows.h>
#include <ctime>
#include <vector>

using namespace std;

class Vertex;

class Edge {
public:
	Edge(int waga, Vertex *one, Vertex *two) // konstruktor
	{
		weight = waga; // nadanie wagi
		first = one; // nadanie wierzcholkow ktore laczy dana krawedz
		last = two;
	}
	int weight; // waga krawedzi
	Vertex *first; // jeden koniec krawedzi
	Vertex *last; // drugi koniec krawedzi
	Edge* posF; // wskaznik na pozycje krawedzi na liscie jednego wierzcholka
	Edge* posL; // wskaznik na pozycje krawedzi na liscie drugiego wierzcholka
	Edge* pos; // wskaznik na pozycje krawedzi na liscie
	string etykieta; // uzywane przy sprawdzaniu czy graf jest spojny
};

class Vertex {
public:
	Vertex(int klucz) // konstruktor
	{
		key = klucz;
	}
	int key; // wartosc wierzcholka
	vector<Edge*> adjList2; // lista sasiedztwa danego wierzcholka
	Vertex* pos; // wskaznik na pozycje wierzcholka na liscie
	string etykieta; // uzywane przy sprawdzaniu czy graf jest spojny
};

class Graph {
public:
	Graph(int n); // konstruktor
	vector<Vertex*> vertexList; // tablica wskaznikow na wierzcholki
	vector<Edge*> edgeList; // tablica wskaznikow na krawedzie
	void addEdge(int waga, int key1, int key2); // dodawanie krawedzi
	void addEdge2(int waga, int indeks1, int indeks2); // dodawanie krawedzi
	bool addVertex(int klucz); // dodawanie wierzcholka
	int findVertex(int klucz); // szukanie wierzcholka
	void vertices(); // wyswietlenie wszystkich wierzcholkow w grafie
	void edges(); // wyswietlenie wszystkich krawedzi w grafie
	void incidentEdges(int indeksV); // wyswietlenie wszystkich krawedzi incydentnych danego wierzcholka
	void endVertices(int indeksE); // wyswietlenie wierzcholkow ktore laczy dana krawedz
	void opposite(int indeksV, int indeksE); // zwrocenie wierzcholka przeciwleglego wzgledem danej krawedzi
	Vertex* opposite(Vertex* V, Edge* E); // zwrocenie wierzcholka przeciwleglego wzgledem danej krawedzi
	void replaceV(int indeksV, int nowyKlucz); // zastapienie elementu na wierzcholku innym
	void removeE(int indeksE); // usuwanie krawedzi
	void removeV(int indeksV); // usuwanie wierzcholka i jego krawedzi incydentnych
};

// funkcja sluzaca do zamieniania dwoch elementow miejscami
void swap(Edge **x, Edge **y)
{
	Edge* temp = *x;
	*x = *y;
	*y = temp;
}

class Heap
{
public:
	Edge* *tablica; // tablica uzywana jako kopiec
	int pojemnosc; // rozmiar tablicy
	int rozmiar; // ilosc elementow w kopcu
public:
	Heap(int edges); // konstruktor
	void MinHeapify(int);
	int parent(int i) { return (i - 1) / 2; }
	void removeMin(); // usuwa pierwszy element z kopca
	Edge* front() { return tablica[0]; } // zwraca wartosc pierwszego elementu
	void insert(Edge *edge); // dodaje nowa wartosc do kopca
};

Heap::Heap(int edges)
{
	rozmiar = 0;
	pojemnosc = edges;
	tablica = new Edge*[pojemnosc];
}

void Heap::insert(Edge *edge)
{
	if (rozmiar == pojemnosc)
	{
		cout << "Kopiec jest pelny!";
		return;
	}

	rozmiar++;
	int i = rozmiar - 1;
	tablica[i] = edge; // umieszczenie nowego elementu na koncu kopca

					   // przywrocenie wlasciwosci kopca
	while (i != 0 && tablica[parent(i)]->weight > tablica[i]->weight)
	{
		swap(&tablica[i], &tablica[parent(i)]);
		i = parent(i);
	}
}

void Heap::removeMin()
{
	//cout << "Rozmiar: " << rozmiar << endl;
	if (rozmiar <= 0) // gdy kopiec jest pusty
		cout << "Kopiec jest pusty!\n";
	if (rozmiar == 1) // gdy w kopcu jest tylko jeden element to zmniejszany jest jego rozmiar
	{
		rozmiar--;
	}

	tablica[0] = tablica[rozmiar - 1]; // ostatni element idzie na pierwsze miejsce
	rozmiar--; // rozmiar zmniejszany o jeden
	MinHeapify(0); // przywrocenie wlasciwosci kopca
}

void Heap::MinHeapify(int i)
{
	int l = 2 * i + 1; // indeks lewego syna
	int r = 2 * i + 2; // indeks prawego syna
	int smallest = i; // najmniejsza wartosc sposrod ocja, lewego syna i prawego syna
	if (l < rozmiar && tablica[l]->weight < tablica[i]->weight) // sprawdzenie czy lewy syn ma mniejsza wartosc niz ojciec
		smallest = l; // lewy syn ustawiany jako smallest
	if (r < rozmiar && tablica[r]->weight < tablica[smallest]->weight) // sprawdzenie czy prawy syn ma mniejsza wartosc niz smallest
		smallest = r; // prawy syn ustawiany jako smallest
	if (smallest != i) // jesli ojciec nie ma najmniejszej wartosci
	{
		swap(&tablica[i], &tablica[smallest]); // zamiana miejscem ojca z synem
		MinHeapify(smallest); // wywolanie rekurencyjne
	}
}

Graph::Graph(int n)
{
	vertexList.resize(n); // ustawienie rozmiaru wektora odpowiadajacego liczbie wierzcholkow
}

bool Graph::addVertex(int klucz)
{
	Vertex *V1 = new Vertex(klucz); // tworzenie nowego wierzcholka
	vertexList[klucz] = V1; // dodanie wierzcholka do listy
	return true;
}

int Graph::findVertex(int klucz)
{
	for (int i = 0; i < vertexList.size(); i++) {
		if (vertexList[i]->key == klucz)
			return i; // zwraca indeks wierzcholka na liscie wierzcholkow
	}
	return -1; // gdy takiego wierzcholka nie ma w grafie
}

void Graph::addEdge2(int waga, int indeks1, int indeks2)
{
	if (indeks1 >= 0 && indeks2 >= 0 && indeks1 < vertexList.size() && indeks2 < vertexList.size()) { // sprawdzenie czy podane zostaly poprawne indeksy
		Edge *E1 = new Edge(waga, vertexList[indeks1], vertexList[indeks2]); // utworzenie krawedzi
		edgeList.push_back(E1); // dodanie krawedzi do listy
		vertexList[indeks1]->adjList2.push_back(E1); // dodanie krawedzi do list krawedzi dla obu wierzcholkow
		vertexList[indeks2]->adjList2.push_back(E1);
		//cout << "Do wierzcholkow " << vertexList[indeks1]->key << " i " << vertexList[indeks2]->key << " dodano krawedz o wartosci " << waga << endl;
	}
}

void Graph::addEdge(int waga, int key1, int key2)
{
	if (findVertex(key1) == -1 || findVertex(key2) == -1) // sprawdzenie czy podane wierzcholki istnieja
		cout << "Nie mozna dodac krawedzi (co najmniej jeden z podanych wierzcholkow nie istnieje)!\n";
	else {
		Edge *E1 = new Edge(waga, vertexList[findVertex(key1)], vertexList[findVertex(key2)]); // utworzenie krawedzi
		edgeList.push_back(E1); // dodanie krawedzi do listy
		E1->pos = edgeList[edgeList.size() - 1]; // wskaznik na pozycje krawedzi na liscie
		vertexList[findVertex(key1)]->adjList2.push_back(E1); // dodanie krawedzi do list krawedzi dla obu wierzcholkow
		E1->posF = vertexList[findVertex(key1)]->adjList2[vertexList[findVertex(key1)]->adjList2.size() - 1]; // wskaznik na pozycje krawedzi na liscie jednego wierzcholka
		vertexList[findVertex(key2)]->adjList2.push_back(E1);
		E1->posF = vertexList[findVertex(key1)]->adjList2[vertexList[findVertex(key1)]->adjList2.size() - 1]; // wskaznik na pozycje krawedzi na liscie drugiego wierzcholka
		cout << "Do wierzcholkow " << key1 << " i " << key2 << " dodano krawedz o wartosci " << waga << endl;
	}
}

void Graph::vertices()
{
	if (vertexList.size() != 0) { // sprawdzenie czy graf ma jakiekolwiek wierzcholki
		cout << endl << "Wszystkie wierzcholki w grafie:\n";
		for (int i = 0; i < vertexList.size(); i++) {
			cout << vertexList[i]->key << " "; // wyswietlanie kolejnych wierzcholkow
		}
		cout << endl;
	}
	else
		cout << "Nie ma zadnych wierzcholkow w grafie!\n";
}

void Graph::edges()
{
	/*
	if (vertexList.size() == 0) { // sprawdzenie czy graf nie jest pusty
	cout << "Graf jest pusty!\n";
	}
	else {
	cout << endl << "Wszystkie krawedzie w grafie:\n";
	for (int i = 0; i < vertexList.size(); i++) { // petla przechodzaca po wierzcholkach
	cout << "Krawedzie incydentne do wierzcholka " << vertexList[i]->key << ": ";
	for (int j = 0; j < vertexList[i]->adjList2.size(); j++) // petla przechodzaca po krawedziach
	cout << vertexList[i]->adjList2.at(j)->weight << " "; // wyswietlanie kolejnych krawedzi
	cout << endl;
	}
	}  */
	if (edgeList.size() == 0) { // sprawdzenie czy graf nie jest pusty
		cout << "Graf nie ma zadnych krawedzi!\n";
	}
	else {
		cout << endl << "Wszystkie krawedzie w grafie:\n";
		for (int i = 0; i < edgeList.size(); i++)  // petla przechodzaca po wierzcholkach
			cout << edgeList[i]->weight << " ";
		cout << endl;
	}
}

void Graph::incidentEdges(int indeksV)
{
	if (indeksV >= 0 && indeksV < vertexList.size()) { // sprawdzenie czy podany zostal poprawny indeks
		if (vertexList[indeksV]->adjList2.size() == 0) // sprawdzenie czy wierzcholek ma jakiekolwiek krawedzie
			cout << "Ten wierzcholek nie ma zadnych krawedzi incydentnych" << endl;
		else
			cout << "Krawedzie incydentne do wierzcholka " << vertexList[indeksV]->key << ": ";
		for (int i = 0; i < vertexList[indeksV]->adjList2.size(); i++) // petla przechodzaca po krawedziach
			cout << vertexList[indeksV]->adjList2.at(i)->weight << " "; // wyswietlanie kolejnych krawedzi
		cout << endl;
	}
	else
		cout << "Podany wierzcholek nie istnieje!\n";
}

void Graph::endVertices(int indeksE)
{
	if (indeksE >= 0 && indeksE < edgeList.size()) { // sprawdzenie czy podany zostal poprawny indeks
		cout << "Krawedz o wadze " << edgeList[indeksE]->weight << " laczy wierzcholki " << edgeList[indeksE]->first->key << " i " << edgeList[indeksE]->last->key << endl;
	}
	else
		cout << "Podana krawedz nie istnieje!\n";
}

void Graph::opposite(int indeksV, int indeksE)
{
	if (indeksV < 0 || indeksV >= vertexList.size())  // sprawdzenie czy podany zostal poprawny indeks wierzcholka
		cout << "Podany wierzcholek nie istnieje!\n";
	else if (indeksE >= 0 && indeksE < vertexList[indeksV]->adjList2.size()) { // sprawdzenie czy podany zostal poprawny indeks krawedzi
		if (vertexList[indeksV] == vertexList[indeksV]->adjList2[indeksE]->first) // sprawdzenie ktory koniec krawedzi zostal podany
			cout << "Wierzcholek przeciwny do podanego to " << vertexList[indeksV]->adjList2[indeksE]->last->key << endl;
		else
			cout << "Wierzcholek przeciwny do podanego to " << vertexList[indeksV]->adjList2[indeksE]->first->key << endl;
	}
	else
		cout << "Podany wierzcholek nie posiada takiej krawedzi!\n";
}

Vertex* Graph::opposite(Vertex* V, Edge* E)
{
	if (E->first->key == V->key) // sprawdzenie ktory wierzcholek koncowy krawedzi zwrocic
		return E->last;
	else
		return E->first;
}

void Graph::replaceV(int indeksV, int nowyKlucz)
{
	if (indeksV >= 0 && indeksV < vertexList.size()) { // sprawdzenie czy podany zostal poprawny indeks
		int staryKlucz = vertexList[indeksV]->key;
		vertexList[indeksV]->key = nowyKlucz; // przypisanie nowej wartosci klucza
		cout << "Nowa wartosc wierzcholka " << staryKlucz << " to " << nowyKlucz << endl;
	}
	else
		cout << "Podany wierzcholek nie istnieje!\n";
}

void Graph::removeE(int indeksE)
{
	if (indeksE >= 0 && indeksE < edgeList.size()) {
		int waga = edgeList[indeksE]->weight;
		for (int i = 0; i < edgeList[indeksE]->first->adjList2.size(); i++) {
			if (edgeList[indeksE]->first->adjList2[i] == edgeList[indeksE]) // znalezienie i usuniecie krawedzi
				edgeList[indeksE]->first->adjList2.erase(edgeList[indeksE]->first->adjList2.begin() + i);
		}
		for (int i = 0; i < edgeList[indeksE]->last->adjList2.size(); i++) {
			if (edgeList[indeksE]->last->adjList2[i] == edgeList[indeksE]) // znalezienie i usuniecie krawedzi
				edgeList[indeksE]->last->adjList2.erase(edgeList[indeksE]->last->adjList2.begin() + i);
		}
		edgeList.erase(edgeList.begin() + indeksE); // usuniecie krawedzi z listy krawedzi
		cout << "Krawedz o wartosci " << waga << " zostala usunieta\n";// sprawdzenie czy podany zostal poprawny indeks
	}
	else {
		cout << "Podana krawedz nie istnieje!\n";
	}
}

void Graph::removeV(int indeksV)
{
	if (indeksV >= 0 && indeksV < vertexList.size()) { // sprawdzenie czy podany zostal poprawny indeks
		int klucz = vertexList[indeksV]->key;
		while (vertexList[indeksV]->adjList2.size() != 0) { // dopoki wierzcholek ma jakiekolwiek krawedzie incydentne
			for (int i = 0; i < edgeList.size(); i++) {
				if (edgeList[i] == vertexList[indeksV]->adjList2[0]) {
					removeE(i); // usuniecie wszystkich krawedzi incydentnych danego wierzcholka
					break;
				}
			}
			//removeE(0); // usuwanie wszystkich krawedzi incydentnych danego wierzcholka
		}
		vertexList.erase(vertexList.begin() + indeksV); // usuniecie danego wierzcholka z listy wierzcholkow
		cout << "Wierzcholek o wartosci " << klucz << " zostal usuniety\n";
	}
	else
		cout << "Podany wierzcholek nie istnieje!\n";
}


void DFS(Graph *graf, Vertex *vertex)
{
	vertex->etykieta = "ODWIEDZONY";
	for (int i = 0; i < vertex->adjList2.size(); i++) { // sprawdzenie kazdej krawedzi incydentnej wierzcholka
		Vertex *V = graf->opposite(vertex, vertex->adjList2[i]);
		if (V->etykieta == "NIEODWIEDZONY") { // sprawdzenie czy wierzcholek zostal juz odwiedzony
			vertex->adjList2[i]->etykieta = "ZNALEZIONY";
			DFS(graf, V); // wywolanie przeszukiwania dla wlasnie odwiedzonego wierzcholka
		}
		else
			vertex->adjList2[i]->etykieta = "POWROTNY";
	}
}

struct wezel {
	Vertex* ojciec; // wskaznik na ojca
	int ranga; // wielkosc drzewa
};

class klaster {
public:
	wezel *W; // tablica wezlow
	klaster(int n) { // konstruktor
		W = new wezel[n];
	}
	~klaster() { // destruktor
		delete[] W;
	}
	void makeK(Vertex *vertex); // utworzenie klastra
	Vertex* findK(Vertex *vertex);
	void unionK(Edge *edge);
	void display(int vertices);
};

void klaster::makeK(Vertex *vertex)
{
	W[vertex->key].ojciec = vertex;
	W[vertex->key].ranga = 0; // ustawienie wielkosci drzewa
}

Vertex* klaster::findK(Vertex *vertex)
{
	if (W[vertex->key].ojciec->key != vertex->key) // jesli wartosc wierzcholka nie jest rowna wartosci ojca
		W[vertex->key].ojciec = findK(W[vertex->key].ojciec); // to funkcja wywolywana jest rekurencyjnie dla ojca
	return W[vertex->key].ojciec; // zwrocenie ojca klastra
}

void klaster::unionK(Edge *edge)
{
	Vertex *V1 = findK(edge->first); // ojciec jednego klastra
	Vertex *V2 = findK(edge->last); // ojciec drugiego klastra
	if (V1 != V2) { // sprawdzenie czy korzenie sa rozne
		if (W[V1->key].ranga > W[V2->key].ranga)
			W[V2->key].ojciec = V1; // gdy ranga V1 wieksza to dolaczamy V2
		else {
			W[V1->key].ojciec = V2;
			if (W[V1->key].ranga == W[V2->key].ranga) // gdy ranga V2 wieksza to dolaczamy V1
				W[V2->key].ranga++;
		}
	}
}

// wyswietlenie zawartosci klastra
void klaster::display(int vertices)
{
	for (int i = 0; i < vertices; i++)
		cout << W[i].ojciec->key << " ";
	cout << endl;
	for (int i = 0; i < vertices; i++)
		cout << W[i].ranga << " ";
	cout << endl;
}

bool DFS(Graph *graf)
{
	int i;
	// ustawienie etykiet wszystkich wierzcholkow i krawedzi jako NIEODWIEDZONY
	for (i = 0; i < graf->vertexList.size(); i++)
		graf->vertexList[i]->etykieta = "NIEODWIEDZONY";
	for (i = 0; i < graf->edgeList.size(); i++)
		graf->edgeList[i]->etykieta = "NIEODWIEDZONY";
	DFS(graf, graf->vertexList[0]); // wywolanie przeszukiwania w glab dla jednego z wierzcholkow
	for (i = 0; i < graf->vertexList.size(); i++) {
		if (graf->vertexList[i]->etykieta != "ODWIEDZONY") {// jesli ktorys z wierzcholkow nie zostal odwiedzony to graf nie jest spojny
			cout << "Graf nie jest spojny!\n";
			return false; // zwraca false gdy graf nie jest spojny
		}
	}
	return true; // zwraca true gdy graf jest spojny
}

void randomize(Graph *graf, Graph *MST, int vertices, float density)
{
	while (1) {
		for (int i = 0; i < vertices; i++) {
			graf->addVertex(i); // dodanie wierzcholka
		}
		int edges = (density / 100.0f * vertices * (vertices - 1)) / 2; // obliczenie ilosci krawedzi w grafie
		for (int i = 0; i < edges; i++) {
			int waga, indeks1, indeks2;
			waga = rand() % 100000; // losowanie wagi krawedzi
			indeks1 = rand() % vertices; // losowanie indeksow wierzcholkow koncowych krawedzi
			indeks2 = rand() % vertices;
			graf->addEdge2(waga, indeks1, indeks2); // dodanie krawedzi
		}
		if (DFS(graf) == true) // sprawdzenie czy graf jest spojny
			break;
		else
			cout << "Jedziemy dalej\n";
	}
}

void kruskal(Graph *graf, Graph *MST, Heap *queue, int vertices)
{
	klaster claster(vertices);
	for (int i = 0; i < graf->vertexList.size(); i++) {
		claster.makeK(graf->vertexList[i]); // wszystkie wezly stanowia osobne klastry
		MST->addVertex(i); // dodanie wierzcholka
	}
	for (int i = 0; i < graf->edgeList.size(); i++) {
		queue->insert(graf->edgeList[i]); // wlozenie wszystkich krawedzi do kolejki priorytetowej
	}
	//queue->display();
	Edge *edge;
	for (int i = 1; i < graf->vertexList.size(); i++) { // n-1 powtorzen, czyli minimalna liczba krawedzi w grafie by byl on spojny
		do {
			edge = queue->front(); // pierwszy element kolejki
			queue->removeMin(); // usuniecie pierwszego elementu
		} while (claster.findK(edge->first) == claster.findK(edge->last)); // dopoki dwie krawedzi zewnetrzne naleza do tego samego klastra
		MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
																		//claster.display(vertices);
		claster.unionK(edge); // polaczenie klastrow zawierajacych dwie krawedzie zewnetrzne
	}
	MST->vertices();
	MST->edges();
}

void prim(Graph *graf, Graph *MST, Heap *queue, int vertices)
{
	int verticesCount = 1; // do sprawdzania czy juz dodano wszystkie wierzcholki
	bool *mstSet = new bool[vertices]; // do sprawdzania czy dany wierzcholek jest juz w drzewie MST
	for (int i = 0; i < graf->vertexList.size(); i++) {
		mstSet[i] = false; // ustawienie wszystkich komorek tablicy mstSet na false
	}
	mstSet[0] = true;
	MST->addVertex(0); // wierzcholek poczatkowy
	for (int i = 0; i < graf->vertexList[0]->adjList2.size(); i++)
		queue->insert(graf->vertexList[0]->adjList2[i]); // dodanie krawedzi incydentnych wierzcholka poczatkowego do kolejki priorytetowej
	Edge *edge;
	Vertex *vertex;
	while (verticesCount < vertices) { // dopoki nie wszystkie krawedzie sa w drzewie MST
									   //queue->display();
									   //cout << endl;
		edge = queue->front(); // pierwszy element kolejki
		queue->removeMin(); // usuniecie pierwszego elementu
		if (mstSet[edge->first->key] == false || mstSet[edge->last->key] == false) { // sprawdzenie czy wierzcholki tej krawedzi sa juz w drzewie MST
			if (mstSet[edge->first->key] == false) { // sprawdzenie ktorej krawedzi nie ma w drzewie
				vertex = graf->vertexList[edge->first->key];
				mstSet[edge->first->key] = true;
				MST->addVertex(edge->first->key); // dodanie wierzcholka do drzewa MST
				verticesCount++; // zwiekszenie liczby wierzcholkow o jeden
				MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
				for (int i = 0; i < vertex->adjList2.size(); i++) {
					if (mstSet[vertex->adjList2[i]->first->key] == false || mstSet[vertex->adjList2[i]->last->key] == false)
						queue->insert(vertex->adjList2[i]); // dodanie krawedzi do kolejki priorytetowej jesli jeszcze sie na niej nie znajduje
				}
			}
			else {
				vertex = graf->vertexList[edge->last->key];
				mstSet[edge->last->key] = true;
				MST->addVertex(edge->last->key); // dodanie wierzcholka do drzewa MST
				verticesCount++; // zwiekszenie liczby wierzcholkow o jeden
				MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
				for (int i = 0; i < vertex->adjList2.size(); i++) {
					if (mstSet[vertex->adjList2[i]->first->key] == false || mstSet[vertex->adjList2[i]->last->key] == false)
						queue->insert(vertex->adjList2[i]); // dodanie krawedzi do kolejki priorytetowej jesli jeszcze sie na niej nie znajduje
				}
			}
		}
	}
	MST->vertices();
	MST->edges();
}

void prim2(Graph *graf, Graph *MST, Heap *queue, int vertices)
{
	int verticesCount = 1; // do sprawdzania czy juz dodano wszystkie wierzcholki
	bool *mstSet = new bool[vertices]; // do sprawdzania czy dany wierzcholek jest juz w drzewie MST
	for (int i = 0; i < graf->vertexList.size(); i++) {
		mstSet[i] = false; // ustawienie wszystkich komorek tablicy mstSet na false
	}
	mstSet[0] = true;
	MST->addVertex(0); // wierzcholek poczatkowy
	for (int i = 0; i < graf->vertexList[0]->adjList2.size(); i++)
		queue->insert(graf->vertexList[0]->adjList2[i]); // dodanie krawedzi incydentnych wierzcholka poczatkowego do kolejki priorytetowej
	Edge *edge;
	Vertex *vertex;
	int size;
	while (verticesCount < vertices) { // dopoki nie wszystkie krawedzie sa w drzewie MST
									   //queue->display();
									   //cout << endl;
		edge = queue->front(); // pierwszy element kolejki
		queue->removeMin(); // usuniecie pierwszego elementu
		if (mstSet[edge->first->key] == false || mstSet[edge->last->key] == false) { // sprawdzenie czy wierzcholki tej krawedzi sa juz w drzewie MST
			if (mstSet[edge->first->key] == false) { // sprawdzenie ktorej krawedzi nie ma w drzewie
				vertex = graf->vertexList[edge->first->key];
				size = vertex->adjList2.size();
				mstSet[edge->first->key] = true;
				MST->addVertex(edge->first->key); // dodanie wierzcholka do drzewa MST
				verticesCount++; // zwiekszenie liczby wierzcholkow o jeden
				MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
				for (int i = 0; i < size; i++) {
					if (mstSet[vertex->adjList2[i]->first->key] == false || mstSet[vertex->adjList2[i]->last->key] == false)
						queue->insert(vertex->adjList2[i]); // dodanie krawedzi do kolejki priorytetowej jesli jeszcze sie na niej nie znajduje
				}
			}
			else {
				vertex = graf->vertexList[edge->last->key];
				size = vertex->adjList2.size();
				mstSet[edge->last->key] = true;
				MST->addVertex(edge->last->key); // dodanie wierzcholka do drzewa MST
				verticesCount++; // zwiekszenie liczby wierzcholkow o jeden
				MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
				for (int i = 0; i < size; i++) {
					if (mstSet[vertex->adjList2[i]->first->key] == false || mstSet[vertex->adjList2[i]->last->key] == false)
						queue->insert(vertex->adjList2[i]); // dodanie krawedzi do kolejki priorytetowej jesli jeszcze sie na niej nie znajduje
				}
			}
		}
	}
}

int main()
{
	int vertices, density; // liczba wierzcholkow i gestosc tworzonego grafu
	cout << "Ile wierzcholkow ma byc w grafie? ";
	cin >> vertices;
	cout << "Jak gesty procentowo ma byc graf? ";
	cin >> density;

	Graph *graf = new Graph(vertices);
	Graph *graf2 = new Graph(vertices);

	Graph *MST = new Graph(vertices); // minimalne drzewo rozpinajace
	Graph *MST2 = new Graph(vertices);

	int edges = (density / 100.0f * vertices * (vertices - 1)) / 2;

	Heap *heap = new Heap(edges);
	Heap *heap2 = new Heap(edges);

	srand(time(NULL));
	randomize(graf, MST, vertices, density);
	randomize(graf2, MST2, vertices, density);

	LARGE_INTEGER frequency; // czestotliwosc
	LARGE_INTEGER t1, t2; // czas
	double Time;
    // wyliczony czas dzialania algorytmu
	QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
	QueryPerformanceCounter(&t1); // uruchomienie timera
	kruskal(graf, MST, heap, vertices);
	QueryPerformanceCounter(&t2); // zatrzymanie timera
	Time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu

	cout << "Czas dla algorytmu Kruskala: " << Time << " ms.\n";

	QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
	QueryPerformanceCounter(&t1); // uruchomienie timera
	prim(graf2, MST2, heap2, vertices);
	QueryPerformanceCounter(&t2); // zatrzymanie timera
	Time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
	cout << "Czas dla algorytmu Prima: " << Time << " ms.\n";

//	graf->addVertex(0);
//	graf->addVertex(1);
//	graf->addVertex(2);
//	graf->addVertex(3);
//	graf->addVertex(4);
//	graf->addVertex(5);
//	graf->addVertex(6);
//	graf->addVertex(7);
//	graf->addVertex(8);
//	graf->addVertex(9);
//	graf->addEdge2(11, 0, 1);
//	graf->addEdge2(15, 1, 2);
//	graf->addEdge2(12, 3, 5);
//	graf->addEdge2(14, 8, 7);
//	graf->addEdge2(18, 6, 3);
//	graf->addEdge2(26, 7, 4);
//	graf->addEdge2(23, 9, 8);
//	graf->addEdge2(28, 0, 6);
//	graf->addEdge2(21, 6, 1);
//	graf->addEdge2(27, 3, 2);
//	graf->addEdge2(30, 9, 5);
//	graf->addEdge2(5, 2, 4);
//	graf->addEdge2(29, 0, 7);
//	graf->addEdge2(37, 5, 2);
//	graf->addEdge2(38, 3, 1);
//	cout << "Graf poczatkowy:\n";
//	graf->vertices();
//	graf->edges();
//	graf2 = graf;
//	cout << "\nMST dla algorytmu Prima:\n";
//	prim(graf, MST, heap, vertices);
//	cout << "\nMST dla algorytmu Kruskala:\n";
//	kruskal(graf2, MST2, heap2, vertices);

}
