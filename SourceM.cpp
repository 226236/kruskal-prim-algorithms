#include <iostream>
#include <cstdlib>
#include <Windows.h>
#include <ctime>
#include <vector>

using namespace std;

class Vertex;

class Edge {
public:
	Edge(int waga, Vertex *one, Vertex *two) // konstruktor
	{
		weight = waga; // nadanie wagi
		first = one; // nadanie wierzcholkow ktore laczy dana krawedz
		last = two;
	}
	int weight; // waga krawedzi
	Vertex *first; // jeden koniec krawedzi
	Vertex *last; // drugi koniec krawedzi
	string etykieta; // uzywane przy sprawdzaniu czy graf jest spojny
};

class Vertex {
public:
	Vertex(int klucz) // konstruktor
	{
		key = klucz;
	}
	int key; // wartosc wierzcholka
	int indeks; // indeks macierzy sasiedztwa powiazany z wierzcholkiem
	string etykieta; // uzywane przy sprawdzaniu czy graf jest spojny
};

class Graph {
public:
	Graph(int n); // konstruktor
	vector<Vertex*> vertexList; // tablica wskaznikow na wierzcholki
	vector<Edge*> edgeList; // tablica wskaznikow na krawedzie
	Edge* **adjMatrix; // macierz sasiedztwa
	int size; // rozmiar macierzy sasiedztwa
	int numberV; // ilosc utworzonych wierzcholkow
	bool addVertex(int klucz); //dodawanie wierzcholka
	void addEdge(int waga, int key1, int key2); // dodawanie krawedzi
	void addEdge2(int waga, int indeks1, int indeks2); // dodawanie krawedzi
	int findVertex(int klucz); // szukanie wierzcholka
	void vertices(); // wyswietlenie wszystkich wierzcholkow w grafie
	void edges(); // wyswietlenie wszystkich krawedzi w grafie
	void incidentEdges(int klucz); // wyswietlenie wszystkich krawedzi incydentnych danego wierzcholka
	void endVertices(Edge *E1); // wyswietlenie wierzcholkow ktore laczy dana krawedz
	Vertex* opposite(int klucz, Edge *E1); // zwrocenie wierzcholka przeciwleglego wzgledem danej krawedzi
	Vertex* opposite(Vertex *V, Edge *E1); // zwrocenie wierzcholka przeciwleglego wzgledem danej krawedzi
	void replaceV(int staryklucz, int nowyKlucz); // zastapienie elementu na wierzcholku innym
	void removeE(int waga); // usuwanie krawedzi
	void removeV(int klucz); // usuwanie wierzcholka i jego krawedzi incydentnych

};

Graph::Graph(int n)
{
	vertexList.resize(n); // ustawienie rozmiaru wektora odpowiadajacego liczbie wierzcholkow

	n = n + 2; // by podana wartosc nie wymagala powiekszania tablicy
	adjMatrix = new Edge* *[n]; // alokacja pamieci
	for (int i = 0; i < n; i++) {
		adjMatrix[i] = new Edge*[n]; // alokacja pamieci
		for (int j = 0; j < n; j++)
			adjMatrix[i][j] = NULL; // wypelnienie tablicy NULLami
	}
	numberV = 0;
	size = n;
}

bool Graph::addVertex(int klucz)
{
	Vertex *V1 = new Vertex(klucz); // tworzenie nowego wierzcholka
	vertexList[klucz] = V1; // dodanie wierzcholka do listy
	if (numberV + 1 == size) { // sprawdzenie czy macierz jest pelna
		size = size * 2; // zwiekszenie rozmiaru tablicy
		Edge* **arrayTemp = new Edge* *[size]; // utworzenie nowej tablicy
		for (int i = 0; i < size; i++) {
			arrayTemp[i] = new Edge*[size]; // alokacja pamieci
			for (int j = 0; j < size; j++)
				arrayTemp[i][j] = NULL; // wypelnienie tablicy NULLami
		}
		for (int i = 0; i < size / 2; i++) {
			for (int j = 0; j < size / 2; j++)
				arrayTemp[i][j] = adjMatrix[i][j]; // wypelnienie tablicy
		}
		adjMatrix = arrayTemp;
	}
	V1->indeks = numberV;
	numberV++;
	//cout << "Dodano wierzcholek o wartosci " << klucz << endl;
	return true;
}

int Graph::findVertex(int klucz)
{
	for (int i = 0; i < vertexList.size(); i++) {
		if (vertexList[i]->key == klucz)
			return i; // zwraca indeks wierzcholka na liscie wierzcholkow
	}
	return -1; // gdy takiego wierzcholka nie ma w grafie
}

void Graph::addEdge2(int waga, int indeks1, int indeks2)
{
	if (indeks1 >= 0 && indeks2 >= 0 && indeks1 < vertexList.size() && indeks2 < vertexList.size()) { // sprawdzenie czy podane zostaly poprawne indeksy
		Edge *E1 = new Edge(waga, vertexList[indeks1], vertexList[indeks2]); // utworzenie krawedzi
		edgeList.push_back(E1); // dodanie krawedzi do listy
		adjMatrix[indeks1][indeks2] = E1; // dodanie do macierzy wskaznika na krawedz
		adjMatrix[indeks2][indeks1] = E1;
		//cout << "Do wierzcholkow " << vertexList[indeks1]->key << " i " << vertexList[indeks2]->key << " dodano krawedz o wartosci " << waga << endl;
	}
}

void Graph::addEdge(int waga, int key1, int key2)
{
	if (findVertex(key1) == -1 || findVertex(key2) == -1) // sprawdzenie czy podane wierzcholki istnieja
		cout << "Nie mozna dodac krawedzi (co najmniej jeden z podanych wierzcholkow nie istnieje)!\n";
	else {
		Edge *E1 = new Edge(waga, vertexList[findVertex(key1)], vertexList[findVertex(key2)]); // utworzenie krawedzi
		adjMatrix[vertexList[findVertex(key1)]->indeks][vertexList[findVertex(key2)]->indeks] = E1; // dodanie do macierzy wskaznika na krawedz
		adjMatrix[vertexList[findVertex(key2)]->indeks][vertexList[findVertex(key1)]->indeks] = E1;
		cout << "Do wierzcholkow " << key1 << " i " << key2 << " dodano krawedz o wartosci " << waga << endl;
	}
}

void Graph::vertices()
{
	if (vertexList.size() != 0) { // sprawdzenie czy graf ma jakiekolwiek wierzcholki
		cout << endl << "Wszystkie wierzcholki w grafie:\n";
		for (int i = 0; i < vertexList.size(); i++)
			cout << vertexList[i]->key << " "; // wyswietlanie kolejnych wierzcholkow
		cout << endl;
	}
	else
		cout << "Nie ma zadnych wierzcholkow w grafie!\n";
}

void Graph::edges()
{ /*
  if (vertexList.size() == 0) { // sprawdzenie czy graf nie jest pusty
  cout << "Graf jest pusty!\n";
  }
  else {
  cout << endl << "Wszystkie krawedzie w grafie:\n";
  for (int i = 0; i < vertexList.size(); i++) { // petla przechodzaca po wierzcholkach
  cout << "Krawedzie incydentne do wierzcholka " << vertexList[i]->key << ": ";
  for (int j = 0; j < vertexList.size(); j++) {// petla przechodzaca po krawedziach
  if (adjMatrix[i][j] != NULL)
  cout << adjMatrix[i][j]->weight << " "; // wyswietlanie kolejnych krawedzi
  }
  cout << endl;
  }
  }
  */
	if (edgeList.size() == 0) { // sprawdzenie czy graf nie jest pusty
		cout << "Graf nie ma zadnych krawedzi!\n";
	}
	else {
		cout << endl << "Wszystkie krawedzie w grafie:\n";
		for (int i = 0; i < edgeList.size(); i++)  // petla przechodzaca po wierzcholkach
			cout << edgeList[i]->weight << " ";
		cout << endl;
	}
}

void Graph::incidentEdges(int key)
{
	int indeks = findVertex(key); // indeks danego wierzcholka
	cout << endl;
	if (indeks != -1) {
		cout << "Krawedzie incydentne do wierzcholka " << vertexList[indeks]->key << ": ";
		for (int i = 0; i < vertexList.size(); i++) {// petla przechodzaca po krawedziach
			if (adjMatrix[vertexList[indeks]->indeks][i] !=NULL)
				cout << adjMatrix[vertexList[indeks]->indeks][i]->weight << " "; // wyswietlanie kolejnych krawedzi
		}
		cout << endl;
	}
	else
		cout << "Podany wierzcholek nie istnieje!\n";
}

void Graph::endVertices(Edge *E1)
{
	if (E1 != NULL) // sprawdzenie czy dana krawedz istnieje
		cout << "Krawedz o wadze " << E1->weight << " laczy wierzcholki " << E1->first->key << " i " << E1->last->key << endl;
	else
		cout << "Podana krawedz nie istnieje!\n";
}

Vertex* Graph::opposite(int klucz, Edge *E1)
{
	int indeks = findVertex(klucz);
	if (indeks == -1) // sprawdzenie czy podany wierzcholek istnieje
		cout << "Podany wierzcholek nie istnieje!\n";
	else {
		for (int i = 0; i < vertexList.size(); i++) {
			if (adjMatrix[indeks][i] == E1) { // sprawdzenie czy podany wierzcholek posiada dana krawedz
				if (E1->first->key == klucz) // sprawdzenie ktory koniec krawedzi zostal podany
					return vertexList[E1->last->indeks]; // zwrocenie wskaznika do danego wierzcholka
				else
					return vertexList[E1->first->indeks];
			}
		}
		cout << "Podany wierzcholek nie posiada takiej krawedzi!\n";
	}
	Vertex *V1 = new Vertex(-1); // wierzcholek zwracany gdy podane zostaly nie odpowiednie dane
	return V1;
}

Vertex* Graph::opposite(Vertex* V, Edge* E)
{
	if (E->first->key == V->key) // sprawdzenie ktory wierzcholek koncowy krawedzi zwrocic
		return E->last;
	else
		return E->first;
}

void Graph::replaceV(int staryKlucz, int nowyKlucz)
{
	int indeks = findVertex(staryKlucz);
	if (indeks == -1) // sprawdzenie czy dany wierzcholek istnieje
		cout << "Podany wierzcholek nie istnieje!\n";
	else {
		vertexList[indeks]->key = nowyKlucz;
		cout << "Nowa wartosc wierzcholka " << staryKlucz << " to " << nowyKlucz << endl;
	}
}

void Graph::removeE(int waga)
{
	int usunieto = 0; // zwiekszany o 1 gdy element zostal usuniety
	if (vertexList.size() == 0) { // sprawdzenie czy graf nie jest pusty
		cout << "Graf jest pusty!\n";
	}
	else {
		for (int i = 0; i < vertexList.size(); i++) { // petla przechodzaca po wierzcholkach
			for (int j = 0; j < vertexList.size(); j++) {// petla przechodzaca po krawedziach
				if (adjMatrix[i][j] != NULL) { // znalezienie i usuniecie krawedzi
					if (adjMatrix[i][j]->weight == waga) {
						adjMatrix[i][j] = NULL;
						usunieto++;
					}
				}
			}
		}
		if (usunieto == 0) // sprawdzenie czy krawedz zostala usunieta (czy istniala krawedz o podanej wadze)
			cout << "Podana krawedz nie istnieje!\n";
		else
			cout << "Krawedz o wartosci " << waga << " zostala usunieta\n";
	}
}

void Graph::removeV(int klucz)
{
	int indeks = findVertex(klucz);
	if (indeks == -1) // sprawdzenie czy dany wierzcholek istnieje
		cout << "Podany wierzcholek nie istnieje!\n";
	else {
		for (int i = 0; i < vertexList.size(); i++) {
			if (adjMatrix[indeks][i] != NULL) // usuniecie wszystkich krawedzi incydentnych danego wierzcholka
				removeE(adjMatrix[indeks][i]->weight);
		}
		// rekonstrukcja macierzy
		for (int i = 0; i < vertexList.size(); i++) {
			for (int j = 0; j < vertexList.size(); j++) {
				if (i > indeks && j > indeks) {
					adjMatrix[i - 1][j - 1] = adjMatrix[i][j];
					adjMatrix[i][j] = NULL;
				}
				else if (i > indeks) {
					adjMatrix[i - 1][j] = adjMatrix[i][j];
					adjMatrix[i][j] = NULL;
				}
				else if (j > indeks) {
					adjMatrix[i][j - 1] = adjMatrix[i][j];
					adjMatrix[i][j] = NULL;
				}
			}
		}
		vertexList.erase(vertexList.begin() + indeks); // usuniecie danego wierzcholka z listy wierzcholkow
		for (int i = indeks; i < vertexList.size(); i++)
			vertexList[i]->indeks--; // uaktualnienie indeksow wierzcholkow
		numberV--;
		cout << "Wierzcholek o wartosci " << klucz << " zostal usuniety\n";
	}
}

// funkcja sluzaca do zamieniania dwoch elementow miejscami
void swap(Edge **x, Edge **y)
{
	Edge* temp = *x;
	*x = *y;
	*y = temp;
}

class Heap
{
public:
	Edge* *tablica; // tablica uzywana jako kopiec
	int pojemnosc; // rozmiar tablicy
	int rozmiar; // ilosc elementow w kopcu
public:
	Heap(int edges); // konstruktor
	void MinHeapify(int);
	int parent(int i) { return (i - 1) / 2; }
	void removeMin(); // usuwa pierwszy element z kopca
	Edge* front() { return tablica[0]; } // zwraca wartosc pierwszego elementu
	void insert(Edge *edge); // dodaje nowa wartosc do kopca
};

Heap::Heap(int edges)
{
	rozmiar = 0;
	pojemnosc = edges;
	tablica = new Edge*[pojemnosc];
}

void Heap::insert(Edge *edge)
{
	if (rozmiar == pojemnosc)
	{
		cout << "Kopiec jest pelny!";
		return;
	}

	rozmiar++;
	int i = rozmiar - 1;
	tablica[i] = edge; // umieszczenie nowego elementu na koncu kopca

					   // przywrocenie wlasciwosci kopca
	while (i != 0 && tablica[parent(i)]->weight > tablica[i]->weight)
	{
		swap(&tablica[i], &tablica[parent(i)]);
		i = parent(i);
	}
}

void Heap::removeMin()
{
	//cout << "Rozmiar: " << rozmiar << endl;
	if (rozmiar <= 0) // gdy kopiec jest pusty
		cout << "Kopiec jest pusty!\n";
	if (rozmiar == 1) // gdy w kopcu jest tylko jeden element to zmniejszany jest jego rozmiar
	{
		rozmiar--;
	}

	tablica[0] = tablica[rozmiar - 1]; // ostatni element idzie na pierwsze miejsce
	rozmiar--; // rozmiar zmniejszany o jeden
	MinHeapify(0); // przywrocenie wlasciwosci kopca
}

void Heap::MinHeapify(int i)
{
	int l = 2 * i + 1; // indeks lewego syna
	int r = 2 * i + 2; // indeks prawego syna
	int smallest = i; // najmniejsza wartosc sposrod ocja, lewego syna i prawego syna
	if (l < rozmiar && tablica[l]->weight < tablica[i]->weight) // sprawdzenie czy lewy syn ma mniejsza wartosc niz ojciec
		smallest = l; // lewy syn ustawiany jako smallest
	if (r < rozmiar && tablica[r]->weight < tablica[smallest]->weight) // sprawdzenie czy prawy syn ma mniejsza wartosc niz smallest
		smallest = r; // prawy syn ustawiany jako smallest
	if (smallest != i) // jesli ojciec nie ma najmniejszej wartosci
	{
		swap(&tablica[i], &tablica[smallest]); // zamiana miejscem ojca z synem
		MinHeapify(smallest); // wywolanie rekurencyjne
	}
}

struct wezel {
	Vertex* ojciec; // wskaznik na ojca
	int ranga; // wielkosc drzewa
};

class klaster {
public:
	wezel *W; // tablica wezlow
	klaster(int n) { // konstruktor
		W = new wezel[n];
	}
	~klaster() { // destruktor
		delete[] W;
	}
	void makeK(Vertex *vertex); // utworzenie klastra
	Vertex* findK(Vertex *vertex);
	void unionK(Edge *edge);
	void display(int vertices);
};

void klaster::makeK(Vertex *vertex)
{
	W[vertex->key].ojciec = vertex;
	W[vertex->key].ranga = 0; // ustawienie wielkosci drzewa
}

Vertex* klaster::findK(Vertex *vertex)
{
	if (W[vertex->key].ojciec->key != vertex->key) // jesli wartosc wierzcholka nie jest rowna wartosci ojca
		W[vertex->key].ojciec = findK(W[vertex->key].ojciec); // to funkcja wywolywana jest rekurencyjnie dla ojca
	return W[vertex->key].ojciec; // zwrocenie ojca klastra
}

void klaster::unionK(Edge *edge)
{
	Vertex *V1 = findK(edge->first); // ojciec jednego klastra
	Vertex *V2 = findK(edge->last); // ojciec drugiego klastra
	if (V1 != V2) { // sprawdzenie czy korzenie sa rozne
		if (W[V1->key].ranga > W[V2->key].ranga)
			W[V2->key].ojciec = V1; // gdy ranga V1 wieksza to dolaczamy V2
		else {
			W[V1->key].ojciec = V2;
			if (W[V1->key].ranga == W[V2->key].ranga) // gdy ranga V2 wieksza to dolaczamy V1
				W[V2->key].ranga++;
		}
	}
}

// wyswietlenie zawartosci klastra
void klaster::display(int vertices)
{
	for (int i = 0; i < vertices; i++)
		cout << W[i].ojciec->key << " ";
	cout << endl;
	for (int i = 0; i < vertices; i++)
		cout << W[i].ranga << " ";
	cout << endl;
}

void DFS(Graph *graf, Vertex *vertex)
{
	vertex->etykieta = "ODWIEDZONY";
	for (int i = 0; i < graf->vertexList.size(); i++) { // sprawdzenie kazdej krawedzi incydentnej wierzcholka
		if (graf->adjMatrix[i][vertex->key] != NULL) {
			Vertex *V = graf->opposite(vertex, graf->adjMatrix[i][vertex->key]);
			if (V->etykieta == "NIEODWIEDZONY") { // sprawdzenie czy wierzcholek zostal juz odwiedzony
				graf->adjMatrix[i][vertex->key]->etykieta = "ZNALEZIONY";
				DFS(graf, V); // wywolanie przeszukiwania dla wlasnie odwiedzonego wierzcholka
			}
			else
				graf->adjMatrix[i][vertex->key]->etykieta = "POWROTNY";
		}
	}
}

bool DFS(Graph *graf)
{
	int i;
	// ustawienie etykiet wszystkich wierzcholkow i krawedzi jako NIEODWIEDZONY
	for (i = 0; i < graf->vertexList.size(); i++)
		graf->vertexList[i]->etykieta = "NIEODWIEDZONY";
	for (i = 0; i < graf->vertexList.size(); i++) {
		for (int j = 0; i < graf->vertexList.size(); i++) {
			if (graf->adjMatrix[i][j] != NULL)
				graf->adjMatrix[i][j]->etykieta = "NIEODWIEDZONY";
		}
	}
	DFS(graf, graf->vertexList[0]); // wywolanie przeszukiwania w glab dla jednego z wierzcholkow
	for (i = 0; i < graf->vertexList.size(); i++) {
		if (graf->vertexList[i]->etykieta != "ODWIEDZONY") {// jesli ktorys z wierzcholkow nie zostal odwiedzony to graf nie jest spojny
			cout << "Graf nie jest spojny!\n";
			return false; // zwraca false gdy graf nie jest spojny
		}
	}
	return true; // zwraca true gdy graf jest spojny
}

void randomize(Graph *graf, Graph *MST, int vertices, float density)
{
	while (1) {
		for (int i = 0; i < vertices; i++) {
			graf->addVertex(i); // dodanie wierzcholka
		}
		int edges = (density / 100.0f * vertices * (vertices - 1)) / 2; // obliczenie ilosci krawedzi w grafie
		for (int i = 0; i < edges; i++) {
			int waga, indeks1, indeks2;
			waga = rand() % 1000; // losowanie wagi krawedzi
			indeks1 = rand() % vertices; // losowanie indeksow wierzcholkow koncowych krawedzi
			indeks2 = rand() % vertices;
			graf->addEdge2(waga, indeks1, indeks2); // dodanie krawedzi
		}
		if (DFS(graf) == true) // sprawdzenie czy graf jest spojny
			break;
		else
			cout << "Jedziemy dalej\n";
	}
}

void kruskal(Graph *graf, Graph *MST, Heap *queue, int vertices)
{
	klaster claster(vertices);
	for (int i = 0; i < graf->vertexList.size(); i++) {
		claster.makeK(graf->vertexList[i]); // wszystkie wezly stanowia osobne klastry
		MST->addVertex(i); // dodanie wierzcholka
	}
	for (int i = 0; i < graf->edgeList.size(); i++)
		queue->insert(graf->edgeList[i]); // wlozenie wszystkich krawedzi do kolejki priorytetowej
										  //queue->display();
	Edge *edge;
	for (int i = 1; i < graf->vertexList.size(); i++) { // n-1 powtorzen, czyli minimalna liczba krawedzi w grafie by byl on spojny
		do {
			edge = queue->front(); // pierwszy element kolejki
			queue->removeMin(); // usuniecie pierwszego elementu
		} while (claster.findK(edge->first) == claster.findK(edge->last)); // dopoki dwie krawedzi zewnetrzne naleza do tego samego klastra
		MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
																		//claster.display(vertices);
		claster.unionK(edge); // polaczenie klastrow zawierajacych dwie krawedzie zewnetrzne
	}
	MST->vertices();
	MST->edges();
}

void prim(Graph *graf, Graph *MST, Heap *queue, int vertices)
{
	//klaster claster(vertices);
	int verticesCount = 1; // do sprawdzania czy juz dodano wszystkie wierzcholki
	bool *mstSet = new bool[vertices]; // do sprawdzania czy dany wierzcholek jest juz w drzewie MST
	for (int i = 0; i < graf->vertexList.size(); i++) {
		//claster.makeK(graf->vertexList[i]); // wszystkie wezly stanowia osobne klastry
		mstSet[i] = false; // ustawienie wszystkich komorek tablicy mstSet na false
	}
	mstSet[0] = true;
	MST->addVertex(0); // wierzcholek poczatkowy
	for (int i = 0; i < graf->vertexList.size(); i++) {
		if (graf->adjMatrix[i][0] != NULL)
			queue->insert(graf->adjMatrix[i][0]); // dodanie krawedzi incydentnych wierzcholka poczatkowego do kolejki priorytetowej
	}
	Edge *edge;
	while (verticesCount < vertices) { // dopoki nie wszystkie krawedzie sa w drzewie MST
									   //queue->display();
									   //cout << endl;
		edge = queue->front(); // pierwszy element kolejki
		queue->removeMin(); // usuniecie pierwszego elementu
		if (mstSet[edge->first->key] == false || mstSet[edge->last->key] == false) { // sprawdzenie czy wierzcholki tej krawedzi sa juz w drzewie MST
			if (mstSet[edge->first->key] == false) { // sprawdzenie ktorej krawedzi nie ma w drzewie
				mstSet[edge->first->key] = true;
				MST->addVertex(edge->first->key); // dodanie wierzcholka do drzewa MST
				verticesCount++; // zwiekszenie liczby wierzcholkow o jeden
				MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
				for (int i = 0; i < graf->vertexList.size(); i++) {
					if (graf->adjMatrix[i][edge->first->key] != NULL) {
						if (mstSet[graf->adjMatrix[i][edge->first->key]->first->key] == false || mstSet[graf->adjMatrix[i][edge->first->key]->last->key] == false)
							queue->insert(graf->adjMatrix[i][edge->first->key]); // dodanie krawedzi do kolejki priorytetowej jesli jeszcze sie na niej nie znajduje
					}
				}
			}
			else {
				mstSet[edge->last->key] = true;
				MST->addVertex(edge->last->key); // dodanie wierzcholka do drzewa MST
				verticesCount++; // zwiekszenie liczby wierzcholkow o jeden
				MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
				for (int i = 0; i < graf->vertexList.size(); i++) {
					if (graf->adjMatrix[i][edge->last->key] != NULL) {
						if (mstSet[graf->adjMatrix[i][edge->last->key]->first->key] == false || mstSet[graf->adjMatrix[i][edge->last->key]->last->key] == false)
							queue->insert(graf->adjMatrix[i][edge->last->key]); // dodanie krawedzi do kolejki priorytetowej jesli jeszcze sie na niej nie znajduje
					}
				}
			}
		}
	}
	MST->vertices();
	MST->edges();
}

int main()
{
	int vertices, density; // liczba wierzcholkow i gestosc tworzonego grafu
	cout << "Ile wierzcholkow ma byc w grafie? ";
	cin >> vertices;
	cout << "Jak gesty procentowo ma byc graf? ";
	cin >> density;

	Graph *graf = new Graph(vertices);
	Graph *graf2 = new Graph(vertices);

	Graph *MST = new Graph(vertices); // minimalne drzewo rozpinajace
	Graph *MST2 = new Graph(vertices);

	int edges = (density / 100.0f * vertices * (vertices - 1)) / 2;

	Heap *heap = new Heap(edges);
	Heap *heap2 = new Heap(edges);

	srand(time(NULL));
	randomize(graf, MST, vertices, density);
	randomize(graf2, MST2, vertices, density);

	LARGE_INTEGER frequency; // czestotliwosc
	LARGE_INTEGER t1, t2; // czas
	double Time; // wyliczony czas dzialania algorytmu
	QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
	QueryPerformanceCounter(&t1); // uruchomienie timera
	kruskal(graf, MST, heap, vertices);
	QueryPerformanceCounter(&t2); // zatrzymanie timera
	Time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
	cout << "Czas dla algorytmu Kruskala: " << Time << " ms.\n";

	QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
	QueryPerformanceCounter(&t1); // uruchomienie timera
	prim(graf2, MST2, heap2, vertices);
	QueryPerformanceCounter(&t2); // zatrzymanie timera
	Time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
	cout << "Czas dla algorytmu Prima: " << Time << " ms.\n";


//	graf->addVertex(0);
//	graf->addVertex(1);
//	graf->addVertex(2);
//	graf->addVertex(3);
//	graf->addVertex(4);
//	graf->addVertex(5);
//	graf->addVertex(6);
//	graf->addVertex(7);
//	graf->addVertex(8);
//	graf->addVertex(9);
//	graf->addEdge2(11, 0, 1);
//	graf->addEdge2(15, 1, 2);
//	graf->addEdge2(12, 3, 5);
//	graf->addEdge2(14, 8, 7);
//	graf->addEdge2(18, 6, 3);
//	graf->addEdge2(26, 7, 4);
//	graf->addEdge2(23, 9, 8);
//	graf->addEdge2(28, 0, 6);
//	graf->addEdge2(21, 6, 1);
//	graf->addEdge2(27, 3, 2);
//	graf->addEdge2(30, 9, 5);
//	graf->addEdge2(5, 2, 4);
//	graf->addEdge2(29, 0, 7);
//	graf->addEdge2(37, 5, 2);
//	graf->addEdge2(38, 3, 1);
//	cout << "Graf poczatkowy:\n";
//	graf->vertices();
//	graf->edges();
//	graf2 = graf;
//	cout << "\nMST dla algorytmu Prima:\n";
//	prim(graf, MST, heap, vertices);
//	cout << "\nMST dla algorytmu Kruskala:\n";
//	kruskal(graf2, MST2, heap2, vertices);

}
